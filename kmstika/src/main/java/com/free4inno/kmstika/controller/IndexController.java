package com.free4inno.kmstika.controller;

import com.free4inno.kmstika.service.AttachmentTikaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Author HUYUZHU.
 * Date 2021/3/26 11:34.
 */

@Controller
public class IndexController {
    @Autowired
    AttachmentTikaService service;

    @ResponseBody
    @RequestMapping("")
    public String index() {
        return service.parseAll();
    }

}
