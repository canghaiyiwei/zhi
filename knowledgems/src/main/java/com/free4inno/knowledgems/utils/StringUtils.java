package com.free4inno.knowledgems.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * Author: HaoYi.
 * Date: 2020/9/19.
 */

@Slf4j
public class StringUtils {

    /**
     * @param str 从数据库中取出要返回前端的字符串
     * @return 将双引号替换完成的字符串, 将换行符替换成<br />
     */
    public static String inputStringFormat(String str) {
        log.info("StringUtils" + "----in----" + "将字符串格式化返回前端" + "----");
        if (str == null || str.length() == 0) {
            log.info("StringUtils" + "----out----" + "返回\"\"，字符串为空" + "----");
            return "";
        }
        log.info("StringUtils" + "----out----" + "返回格式化后的字符串，已将换行替换为<br/>标签" + "----");
        return str.replaceAll("\r\n", "<br/>")
                .replaceAll("\n\r", "<br/>")
                .replaceAll("\n", "<br/>")
                .replaceAll("\r", "<br/>");
//        .replaceAll("\"", "&quot;")
    }

    // 生成随机字母数字组合的字符串（用于openApi生成随机appKey）
    public static String getRandomString(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            if ("char".equalsIgnoreCase(charOrNum)) {
                int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (choice + random.nextInt(26));
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }
}
