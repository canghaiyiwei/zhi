function centerModal() {
    $(this).css('display', 'block');
    var $dialog  = $(this).find(".modal-dialog"),
        offset   = ($(window).height() - $dialog.height()) / 2

    $dialog.css("margin-top", offset - 15);
}
function loading(action) {
    /*
     * loading animation
     * @ author HY
     * action show:显示加载动画  reset:隐藏加载动画
     *
     */
    var $loadingModal = $('#loading-modal'),
        html

    //TODO “正在加载”替换为知了的动画
    if (!$loadingModal.length) {
        html =
            [
                '<div class="modal fade" data-backdrop="static" data-keyboard="false" id="loading-modal" tabindex="-1">',
                '<div class="modal-dialog">',
                '<div class="modal-body">',
                '<div class="front-loading">',
                '<img src="/assets/images/loading.gif"/>',
                '</div>',
                '</div>',
                '</div>',
                '</div>'
            ].join("");

        $('body').append(html)

        $loadingModal = $('#loading-modal').on('show.bs.modal', centerModal);
    }

    if (action === "show") {
        $loadingModal.modal('show')
    }

    if (action === "reset") {
        //chrome中modal('show')和modal('hide')运行要有间隔，不然会出问题
        setTimeout(function() {
                $loadingModal.modal('hide');
            },
            500);
    }
}