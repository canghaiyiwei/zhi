function getDivPageNumHtml(curPage, endPage, funcName){
    /*
     * creates pagination
     * @ author HY
     * @ return pageHtml
     * curPage 当前页数
     * endPage 最终页数
     * funcName 异步加载的方法名 异步方法包括两个参数funcName(page)
     *
     */
    var pageHtml = "";
    var showPage = 6;

    if ($(window).width() <= 991){
        showPage = 4;
    }
    if ($(window).width() <= 767){
        showPage = 2;
    }
    if ($(window).width() <= 575){
        showPage = 1;
    }

    pageHtml += '<ul class="re-pagination" style="flex-wrap: wrap">';

    // 仅有一页时
    if (endPage == 1) {
        pageHtml += "<li class=\"re-pagination-icon disabled\"><a><span class=\"icon pe-7s-angle-left\"></span></a></li>" +
            "<li><a>1</a></li>" +
            "<li class=\"re-pagination-icon disabled\"><a><span class=\"icon pe-7s-angle-right\"></span></a></li></ul>";
        return pageHtml;
    }

    // 具有多页时
    if (curPage == 1) {
        // 禁用左箭头减少页码
        pageHtml += "<li class=\"re-pagination-icon disabled\"><a><span class=\"icon pe-7s-angle-left\"></span></a></li>";
    } else {
        // 启用左箭头减少页码
        pageHtml += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:" + funcName + "(" + (curPage - 1) + ");\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
    }

    var tmpBegin = 1;
    var tmpEnd = 1;
    // var tmpSum = (endPage - endPage % showPage) / showPage;

    // 以下部分为计算分页显示多少中间页
    if (endPage <= showPage) {
        tmpBegin = 1;
        tmpEnd = endPage;
    } else {
        if (curPage < showPage) {
            tmpBegin = 1;
            tmpEnd = showPage
        } else if (curPage > endPage - showPage + 1) {
            tmpBegin = endPage - showPage + 1;
            tmpEnd = endPage;
        } else {
            tmpBegin = curPage - Math.ceil(showPage / 2) + 1;
            tmpEnd = parseInt(curPage) + parseInt(Math.floor(showPage / 2));
        }
    }

    // 首页 & 省略号
    if (tmpBegin > 2) {
        pageHtml += "<li><a href=\"javascript:" + funcName + "(1);\">1</a></li>";
        pageHtml += "<li style=\"cursor:default\"><a>...</a></li>"
    } else if (tmpBegin == 2) {
        pageHtml += "<li><a href=\"javascript:" + funcName + "(1);\">1</a></li>";
    }

    for (var i = tmpBegin; i <= tmpEnd; i++) {
        if (i == curPage) {
            pageHtml += "<li class=\"active\" style=\"cursor:default\"><a>" + i + "</a>";
            continue;
        }
        pageHtml += "<li><a href=\"javascript:"+ funcName + "(" + i + ");\">" + i + "</a>";
    }

    // 省略号 & 尾页
    if (tmpEnd < endPage - 1) {
        pageHtml += "<li style=\"cursor:default\"><a>...</a></li>"
        pageHtml += "<li><a href=\"javascript:" + funcName + "(" + endPage + ");\">" + endPage + "</a></li>"
    } else if (tmpEnd == endPage - 1) {
        pageHtml += "<li><a href=\"javascript:" + funcName + "(" + endPage + ");\">" + endPage + "</a></li>"
    }

    if (curPage != endPage) {
        pageHtml += "<li class=\"re-pagination-icon\"><a href=\"javascript:" + funcName + "(" + (Number(curPage) + Number(1)) + ");\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
    } else {
        pageHtml += "<li class=\"re-pagination-icon disabled\"><a><span class=\"icon pe-7s-angle-right\"></span></a></li>";
    }

    //有...的情况，加入一个页码跳转功能
    if (tmpBegin > 2 || tmpEnd < endPage - 1) {
        pageHtml += "<li><a><input id=\"pagenum-jumpPage\" class=\"re-pagination-input\" oninput=\"value=value.replace(/[^\\r\\n0-9]/g,'')\" onkeydown=\"jumpPageKeydown()\"></a></li><li><a id=\"pagenum-jumpPageButton\" href=\"javascript:jumpPage("+endPage+","+funcName+")\">跳转</a></li></ul></div>"
    }else {
        pageHtml += "</ul></div>"
    }

    return pageHtml;
}

function jumpPage(endPage, funcName){
    var jumpPageNum = parseInt(document.getElementById("pagenum-jumpPage").value);
    // console.log(jumpPageNum)
    if(isNaN(jumpPageNum) || typeof jumpPageNum != "number" || jumpPageNum == 0){
        $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写一个正整数页码值'});
    }else if(jumpPageNum <= endPage) {
        funcName(jumpPageNum)
    }else {
        $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写小于尾页的页码值'});
    }
}

function jumpPageKeydown(){
    $("#pagenum-jumpPage").keydown(function (event){
        if (event.keyCode == 13){
            $("#pagenum-jumpPageButton")[0].click();
        }
    });
}



