<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/10/14
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>登录-知了[团队知识管理]</title>
    <style>
        body{background-color: #1b1b1b}
    </style>
</head>
<body>
<div class="login-box" id="login">
    <div class="login-box text-center text-white" style="padding-top: 120px;">
        <h1 class="text-white mb-30">登&emsp;录</h1>
        <form action="login" method="post" onsubmit="return preSubmit()" class="re-form">
            <div class="re-form-group-md" id="account-div">
                <input type="text" class="form-control" id="account-input" name="account"
                       placeholder="手机号/邮箱" onKeypress="javascript:if(event.keyCode == 32)event.returnValue = false;">
            </div>
            <div class="re-form-group-md" id="psw-div">
                <input type="password" class="form-control" id="psw-input" name="psw"
                       placeholder="密码">
            </div>
            <div class="re-form-group-md">
                <button type="submit" class="re-btn re-btn-block re-btn-lg">登录</button>
            </div>
            <div class="re-form-group-md">
                <div class="d-flex justify-content-between">
                    <div class="d-flex" style="/*实现垂直居中*/align-items: center;color: #b9b9b9">
                        <input type="checkbox" id="rmb-account">&nbsp记住用户
                    </div>
                    <div class="login-white">
                        <a href="javascript:void(0);" onclick="showForgetLabel()">忘记密码</a>&nbsp|&nbsp<a href="/signup">注册</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="forgetLabel" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="wrong_header">&nbsp&nbsp忘记密码</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <p>请发邮件至管理员邮箱</p>
                        <p id="mail" style="font-weight:bold"></p>
                        <p>申请重置密码</p>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagIdWrong" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancelWrong" class="btn btn-primary" data-dismiss="modal"
                        onclick="sendEmail()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<c:import url="/template/_include_js.jsp"/>
<script type="text/javascript">
    let userMail = "";

    (function () {
        if($.cookie('account') != undefined) {
            $('#account-input').val($.cookie('account'));
            $('#rmb-account').attr('checked', 'true');
        }
        $('#account-input').blur(accountInputBlur);
        $('#psw-input').blur(pswInputBlur);
        if ("${wrongOfEmailOrPsw}"==="true")
        {
            $.fillTipBox({type:'danger', icon:'glyphicon-alert', content:'账号错误！'});
        }
        //注册
        <%--if ("${signFin}"==="true")--%>
        <%--{--%>
        <%--    $.fillTipBox({type:'success', icon:'glyphicon-ok-sign', content:'注册成功请登录！'});--%>
        <%--}--%>
    })();

    function accountInputBlur(){
        if($('#account-div').hasClass("has-error")) {
            $('#account-div').removeClass("has-error");
        }
    }

    function pswInputBlur() {
        if($('#psw-div').hasClass("has-error")) {
            $('#psw-div').removeClass("has-error");
        }
        /*
        if($('#psw-input').val() == "") {
            $('#psw-div').addClass("has-error");
            $.fillTipBox({type:'danger', icon:'glyphicon-alert', content:'请输入密码！'});
        }
        */
    }

    function preSubmit() {
        if(!isMail($('#account-input').val()) && !isMobile($('#account-input').val())) {
            $('#account-div').addClass("has-error");
            $.fillTipBox({type:'danger', icon:'glyphicon-alert', content:'手机号/邮箱格式非法！'});
            return false;
        }
        if($('#psw-input').val() == "") {
            $('#psw-div').addClass("has-error");
            $.fillTipBox({type:'danger', icon:'glyphicon-alert', content:'请输入密码！'});
            return false;
        }
        $('#account-input').blur();
        $('#psw-input').blur();
        if($('#rmb-account').is(':checked')) {
            $.cookie('account', $('#account-input').val());
        } else {
            $.removeCookie('account');
        }
        if($('#account-div').hasClass("has-error") || $('#psw-div').hasClass("has-error")) {
            return false;
        } else {
            $('#psw-input').val($.md5($('#psw-input').val()));
            return true;
        }
    }

    function showForgetLabel(){
        $.ajax({
            type: "get",
            url: "/user/getEmail",
            contentType: "application/json;charset=utf-8",
            success: function (mail) {
                document.getElementById("mail").innerHTML = mail;
                userMail = mail;
            }
        });
        $('#forgetLabel').modal();
    }

    function sendEmail(){
        const mail = userMail;
        location="mailto:" + mail+ "?subject=test&cc=" + mail + "&subject=申请重置密码&body=我的账户名称为：";
    }

    //检查手机和邮箱格式
    function isMobile(phone) {
        let isPhone = 1;
        var phone = phone;
        if (!(phone.length == 11)) {
            isPhone = 0;
            return isPhone;
        }
        if (!(phone.substring(0, 1) == 1)) {
            isPhone = 0;
            return isPhone;
        }
        for (i = 0; i < phone.length; i++) {
            num = phone.substring(i, i + 1);
            if (!(num == 0 || num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9)) {
                isPhone = 0;
                return isPhone;
            }
        }
        return isPhone;
    }

    function isMail(mail) {
        let isMail = 0;
        var mail = mail;
        const mailLast = mail.substring(mail.length - 1, mail.length);
        for (i = 0; i < mail.length; i++) {
            var word = mail.substring(i, i + 1);
            if (!((word >= 'a' && word <= 'z') || (word >= 'A' && word <= 'Z') || (word >= '0' && word <= '9') || (word == "@") || (word == "_") || (word == "-") || (word == "."))) {
                return isMail;
            }
        }
        if (mailLast == "@") {
            return isMail;
        }
        if (mail.substring(0, 1) == "@") {
            return isMail;
        }
        for (i = 0; i < mail.length; i++) {
            num = mail.substring(i, i + 1);
            if (num == "@") {
                isMail = isMail + 1;
            }
        }
        return isMail;
    }

</script>
</body>
</html>
