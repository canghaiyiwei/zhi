<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/10/7
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Amdesk - Help Center HTML template for your digital products">
<meta name="keywords" content="helpdesk, forum, template, HTML template, responsive, clean">
<meta name="author" content="nK">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="../assets/images/titlelogo.png">

<!-- Google Fonts -->
<%--<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700%7cMaven+Pro:400,500,700" rel="stylesheet"><!-- %7c -->--%>
<link rel="stylesheet" type="text/css" href="../assets/vendor/googlefonts.css">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="../assets/vendor/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-grid.css">
<link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-reboot.css">

<!-- Fancybox -->
<!--<link rel="stylesheet" type="text/css" href="../assets/vendor/fancybox/dist/jquery.fancybox.min.css">-->

<!-- Pe icon 7 stroke -->
<link rel="stylesheet" type="text/css" href="../assets/vendor/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

<!-- Swiper -->
<%--<link rel="stylesheet" type="text/css" href="../assets/vendor/swiper/dist/css/swiper.min.css">--%>

<!-- Bootstrap Select -->
<link rel="stylesheet" type="text/css" href="../assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css">

<!-- Dropzone -->
<!--<link rel="stylesheet" type="text/css" href="../assets/vendor/dropzone/dist/min/dropzone.min.css">-->

<!-- Quill -->
<!--<link rel="stylesheet" type="text/css" href="../assets/vendor/quill/dist/quill.snow.css">-->

<!-- Font Awesome -->
<!--<script defer src="../assets/vendor/fontawesome-free/js/all.js"></script>-->
<!--<script defer src="../assets/vendor/fontawesome-free/js/v4-shims.js"></script>-->

<!-- Amdesk -->
<%--<link rel="stylesheet" href="../assets/css/amdesk.css">--%>

<!-- Newcss -->
<link rel="stylesheet" type="text/css" href="../newcss/css/newCSS.css?v=2022.03.08.0142">

<!-- Custom Styles -->
<!--<link rel="stylesheet" href="../assets/css/custom.css">-->

<!-- jQuery -->
<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>

<!-- front -->
<link rel="stylesheet" type="text/css" href="../assets/vendor/newfront/front.css">
