#!/bin/bash

echo "start to create index for zhies knowledge_ms_v3"
es_status='000'
while(($es_status!='200'))
do
    es_status=`curl -I -m 10 -o /dev/null -s -w %{http_code} zhies-node-1:9200`
done
curl -XPUT "zhies-node-1:9200/knowledge_ms_v3/" \
 -d '{ "settings": { "number_of_shards": "3", "number_of_replicas": "1", "analysis": { "analyzer": { "douhao": { "type": "pattern", "pattern": "," } } } }, "mappings": { "resource": { "properties": { "@timestamp": { "type": "date" }, "@version": { "type": "text", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } } }, "attachment": { "type": "nested", "properties": { "resourceId": { "type": "keyword" }, "attachmentId": { "type": "keyword" }, "createTime": { "type": "keyword" }, "name": { "type": "keyword" }, "url": { "type": "keyword" }, "attach_text": { "type": "text" }, "status": { "type": "keyword" } } }, "comment": { "type": "nested", "properties": { "resourceId": { "type": "keyword" }, "user_id": { "type": "keyword" }, "comment": { "type": "text" }, "time": { "type": "keyword" }, "userName": { "type": "keyword" }, "picName": { "type": "keyword" } } }, "collection": { "type": "keyword" }, "create_time": { "type": "date" }, "crop_id": { "type": "long" }, "edit_time": { "type": "keyword" }, "group_id": { "type": "text", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } }, "analyzer": "douhao" }, "id": { "type": "keyword" }, "label_id": { "type": "text", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } }, "analyzer": "douhao" }, "opposition": { "type": "keyword" }, "pageview": { "type": "keyword" }, "permissionId": { "type": "keyword" }, "recognition": { "type": "keyword" }, "superior": { "type": "keyword" }, "text": { "type": "text", "term_vector": "with_positions_offsets", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } }, "analyzer": "ik_max_word", "search_analyzer": "ik_smart" }, "title": { "type": "text", "term_vector": "with_positions_offsets", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } }, "analyzer": "ik_max_word", "search_analyzer": "ik_smart" }, "type": { "type": "text", "fields": { "keyword": { "type": "keyword", "ignore_above": 256 } } }, "user_id": { "type": "keyword" } } } } }' \
 -H 'Content-Type: application/json'
echo "success create index for zhies"
echo "end"
